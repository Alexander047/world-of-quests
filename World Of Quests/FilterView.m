//
//  FilterView.m
//  World Of Quests
//
//  Created by Alexander on 16.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "FilterView.h"

@implementation FilterView {
    UITapGestureRecognizer *tap;
    BOOL isShown;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    if (!tap) {
        tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped)];
        tap.numberOfTapsRequired = 1;
        tap.numberOfTouchesRequired = 1;
        [self addGestureRecognizer:tap];
    }
}

- (void)showFilter {
    isShown = YES;
    [self.arrowImageView setTransform:CGAffineTransformMakeRotation(M_PI)];
    [self.delegate filterWillShow];
}

- (void)hideFilter {
    [self.arrowImageView setTransform:CGAffineTransformMakeRotation(0)];
    [self.delegate filterWillHide];
}

- (void)labelTapped {
    isShown = !isShown;
    if (isShown) {
        [self.arrowImageView setTransform:CGAffineTransformMakeRotation(M_PI)];
        [self.delegate filterWillShow];
    } else {
        [self.arrowImageView setTransform:CGAffineTransformMakeRotation(0)];
        [self.delegate filterWillHide];
    }
}

@end
