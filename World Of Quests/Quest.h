//
//  Quest.h
//  World Of Quests
//
//  Created by Alexander on 17.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuestTableViewCell.h"

@interface Quest : NSObject

@property (weak, nonatomic) UITableViewCell *curCell;

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSNumber *questId;
@property (strong, nonatomic) NSString *descriptionShort;
@property (strong, nonatomic) NSString *descriptionFull;
@property (strong, nonatomic) NSNumber *level;
@property (strong, nonatomic) NSNumber *minPlayers;
@property (strong, nonatomic) NSNumber *maxPlayers;
@property (strong, nonatomic) NSNumber *duration;
@property (strong, nonatomic) NSNumber *rating;
@property (strong, nonatomic) NSNumber *sortApp;
@property (strong, nonatomic) NSString *photo1;
@property (strong, nonatomic) NSString *photo2;
@property (strong, nonatomic) NSString *photo3;
@property (strong, nonatomic) NSString *url;
@property (strong, nonatomic) NSArray *reviews;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *company;
@property (strong, nonatomic) NSString *adress;
@property (strong, nonatomic) NSString *route;
@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *www;
@property (strong, nonatomic) NSArray *categories;
@property (strong, nonatomic) UIImage *firstImage;
@property (strong, nonatomic) NSString *bookingLink;
@property (strong, nonatomic) NSString *metro;

+ (instancetype)questFromDic:(NSDictionary *)dic;
- (void)setupCell;
- (void)pauseImageDownload;

@end
