//
//  City.m
//  World Of Quests
//
//  Created by Alexander on 13.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "City.h"

@implementation City

+ (instancetype)cityFromDic:(NSDictionary *)dic {
    City *curCity = [City new];
    curCity.cityId = dic[@"id"];
    curCity.sort = dic[@"sort"];
    curCity.name = dic[@"name"];
    curCity.country = dic[@"country"];
    curCity.fullUrl = dic[@"full_url"];
    curCity.latitude = dic[@"latitude"];
    curCity.longitude = dic[@"longitude"];
    curCity.woqLink = dic[@"woq_link"];
    curCity.isShowingInMenu = (BOOL)dic[@"is_showing_in_menu"];
    return curCity;
}

@end
