//
//  QuestTableViewCell.m
//  World Of Quests
//
//  Created by Alexander on 04.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "QuestTableViewCell.h"

@implementation QuestTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initGui];
}

- (void)initGui {
    self.mainView.layer.cornerRadius = 4.0f;
    self.mainView.layer.masksToBounds = YES;
}

@end
