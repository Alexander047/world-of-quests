//
//  FilterTableView.h
//  World Of Quests
//
//  Created by Alexander on 21.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterTableView : UITableView

@property (strong, nonatomic) NSNumber *selectedCategory;
@property (strong, nonatomic) UIImageView *checkImageView;

@end
