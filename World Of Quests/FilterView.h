//
//  FilterView.h
//  World Of Quests
//
//  Created by Alexander on 16.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FilterViewDelegate <NSObject>

- (void)filterWillShow;
- (void)filterWillHide;

@end

@interface FilterView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (nonatomic,strong) id delegate;

- (void)showFilter;
- (void)hideFilter;

@end
