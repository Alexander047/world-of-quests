//
//  GlobalActivity.m
//  World Of Quests
//
//  Created by Alexander on 06.01.16.
//  Copyright © 2016 Alexander. All rights reserved.
//

#import "GlobalActivity.h"
#import "GlobalActivityView.h"

@interface GlobalActivity ()

@property (strong, nonatomic) GlobalActivityView *mainView;
@property (weak, nonatomic) UIActivityIndicatorView *activityIndicator;

@end

@implementation GlobalActivity

+ (instancetype)sharedInstance {
    static GlobalActivity *model = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        model = [GlobalActivity new];
        [model initGui];
    });
    return model;
}

- (void)initGui {
    self.mainView = [[NSBundle mainBundle] loadNibNamed:@"GlobalActivityView" owner:self options:nil].lastObject;
}

- (void)showActivity {
    [[UIApplication sharedApplication].keyWindow addSubview:self.mainView];
    [self.mainView.activityIndicator startAnimating];
}

- (void)hideActivity {
    [self.mainView removeFromSuperview];
    [self.mainView.activityIndicator stopAnimating];
}

@end
