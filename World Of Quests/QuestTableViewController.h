//
//  QuestTableViewController.h
//  World Of Quests
//
//  Created by Alexander on 19.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Quest.h"

@interface QuestTableViewController : UITableViewController

@property (strong, nonatomic) Quest *curQuest;

@end
