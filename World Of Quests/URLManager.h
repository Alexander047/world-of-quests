//
//  URLManager.h
//  World Of Quests
//
//  Created by Alexander on 14.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLManager : NSObject

+ (instancetype)sharedInstance;
- (NSURL *)urlWithPredicateFromString:(NSString *)string;
- (void)updatePredicate:(NSString *)predicate;

@end
