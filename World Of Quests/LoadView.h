//
//  LoadView.h
//  World Of Quests
//
//  Created by Alexander on 23.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadView : UIView

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
