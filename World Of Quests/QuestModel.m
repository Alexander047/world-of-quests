//
//  QuestModel.m
//  World Of Quests
//
//  Created by Alexander on 17.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "QuestModel.h"
#import "Quest.h"
#import "URLManager.h"
#import "CitiesModel.h"
#import <AFNetworking/AFNetworking.h>

@interface QuestModel ()

@property (strong, nonatomic) AFHTTPRequestOperation *operation;
@property (strong, nonatomic) NSDictionary *questsDic;
@property (strong, nonatomic) NSArray *questsArray;
@property (strong, nonatomic) NSArray *categoriesArray;
@property (strong, nonatomic) NSArray *recommendedArray;
@property (strong, nonatomic) NSMutableDictionary *imagesDictionary;
@property (strong, nonatomic) void (^recomCallback)(NSArray *, NSError *);
@property (strong, nonatomic) void (^qstsCategCallback)(NSDictionary *, NSArray *, NSError *);

@end

@implementation QuestModel

+ (instancetype)sharedInstance {
    static QuestModel *model = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        model = [QuestModel new];
        
    });
    return model;
}

- (void)clearAndReloadData {
    self.questsDic = nil;
    self.categoriesArray = nil;
    self.recommendedArray = nil;
    self.questsArray = nil;
    self.imagesDictionary = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"categForCity%@", [[CitiesModel sharedInstance] selectedCity]]];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"questsForCity%@", [[CitiesModel sharedInstance] selectedCity]]];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"imagesForCity%@", [[CitiesModel sharedInstance] selectedCity]]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self getCategories];
}

- (void)setRecommendedCallback:(void (^)(NSArray *, NSError *))callback {
    self.recomCallback = callback;
    if (!(self.recommendedArray.count > 0)) {
        if (!self.operation) {
            [self getCategories];
        }
    } else {
        callback(self.recommendedArray, nil);
    }
}

- (void)setQuestsWithCategoriesCallback:(void (^)(NSDictionary *, NSArray *, NSError *))callback {
    self.qstsCategCallback = callback;
    NSData *imagesData = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"imagesForCity%@", [[CitiesModel sharedInstance] selectedCity]]];
    self.imagesDictionary = [[NSMutableDictionary alloc] initWithDictionary:[NSKeyedUnarchiver unarchiveObjectWithData:imagesData]];
    if (!(self.questsDic.allKeys.count > 0)) {
        if (!self.operation) {
            [self getCategories];
        }
    } else {
        callback(self.questsDic, self.categoriesArray, nil);
    }
}

- (void)getCategories {
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    NSString *categKey = [NSString stringWithFormat:@"categForCity%@", [[CitiesModel sharedInstance] selectedCity]];
    NSString *qstsKey = [NSString stringWithFormat:@"questsForCity%@", [[CitiesModel sharedInstance] selectedCity]];
    if ([usDef objectForKey:categKey]) {
        NSData *arrayData = [usDef objectForKey:categKey];
        self.categoriesArray = [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];
        if ([usDef objectForKey:qstsKey]) {
            arrayData = [usDef objectForKey:qstsKey];
            self.questsArray = [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];
            NSMutableDictionary *qstDic = [NSMutableDictionary new];
            NSMutableArray *recomArray = [NSMutableArray new];
            for (NSDictionary *dic in self.questsArray) {
                Quest *curQuest = [Quest questFromDic:dic];
                [qstDic setObject:curQuest forKey:curQuest.questId];
                if ([curQuest.sortApp floatValue] > 0) {
                    [recomArray addObject:[Quest questFromDic:dic]];
                }
            }
            self.questsDic = [NSDictionary dictionaryWithDictionary:qstDic];
            self.recommendedArray = [NSArray arrayWithArray:recomArray];
            if (self.recomCallback) {
                self.recomCallback(self.recommendedArray, nil);
            } if (self.qstsCategCallback) {
                self.qstsCategCallback(self.questsDic, self.categoriesArray, nil);
            }
        } else {
            __weak typeof(self) weakSelf = self;
            NSURL *url = [[URLManager sharedInstance] urlWithPredicateFromString:@"/app_api/quests.json"];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            self.operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            self.operation.responseSerializer = [AFJSONResponseSerializer serializer];
            [self.operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id respObject) {
                weakSelf.questsArray = respObject;
                NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:weakSelf.questsArray];
                [usDef setObject:dataSave forKey:qstsKey];
                NSMutableDictionary *qstDic = [NSMutableDictionary new];
                NSMutableArray *recomArray = [NSMutableArray new];
                for (NSDictionary *dic in weakSelf.questsArray) {
                    Quest *curQuest = [Quest questFromDic:dic];
                    [qstDic setObject:curQuest forKey:curQuest.questId];
                    if ([curQuest.sortApp floatValue] > 0) {
                        [recomArray addObject:[Quest questFromDic:dic]];
                    }
                }
                weakSelf.questsDic = [NSDictionary dictionaryWithDictionary:qstDic];
                weakSelf.recommendedArray = [NSArray arrayWithArray:recomArray];
                if (weakSelf.recomCallback) {
                    weakSelf.recomCallback(weakSelf.recommendedArray, nil);
                } if (weakSelf.qstsCategCallback) {
                    weakSelf.qstsCategCallback(weakSelf.questsDic, weakSelf.categoriesArray, nil);
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error){
                NSLog(@"Oops, something went wrong: %@", [error localizedDescription]);
                if (weakSelf.recomCallback) {
                    weakSelf.recomCallback(nil, error);
                } if (weakSelf.qstsCategCallback) {
                    weakSelf.qstsCategCallback(nil, nil, error);
                }
            }];
            [self.operation start];
        }
    } else {
        __weak typeof(self) weakSelf = self;
        NSURL *url = [[URLManager sharedInstance] urlWithPredicateFromString:@"/app_api/categories.json"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        self.operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        self.operation.responseSerializer = [AFJSONResponseSerializer serializer];
        [self.operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *oper, id responseObject) {
            weakSelf.categoriesArray = responseObject;
            NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:weakSelf.categoriesArray];
            [usDef setObject:dataSave forKey:categKey];
            NSURL *url = [[URLManager sharedInstance] urlWithPredicateFromString:@"/app_api/quests.json"];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            weakSelf.operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            weakSelf.operation.responseSerializer = [AFJSONResponseSerializer serializer];
            [weakSelf.operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id respObject) {
                weakSelf.questsArray = respObject;
                NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:weakSelf.questsArray];
                [usDef setObject:dataSave forKey:qstsKey];
                NSMutableDictionary *qstDic = [NSMutableDictionary new];
                NSMutableArray *recomArray = [NSMutableArray new];
                for (NSDictionary *dic in weakSelf.questsArray) {
                    Quest *curQuest = [Quest questFromDic:dic];
                    [qstDic setObject:curQuest forKey:curQuest.questId];
                    if ([curQuest.sortApp floatValue] > 0) {
                        [recomArray addObject:[Quest questFromDic:dic]];
                    }
                }
                weakSelf.questsDic = [NSDictionary dictionaryWithDictionary:qstDic];
                weakSelf.recommendedArray = [NSArray arrayWithArray:recomArray];
                if (weakSelf.recomCallback) {
                    weakSelf.recomCallback(weakSelf.recommendedArray, nil);
                } if (weakSelf.qstsCategCallback) {
                    weakSelf.qstsCategCallback(weakSelf.questsDic, weakSelf.categoriesArray, nil);
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error){
                NSLog(@"Oops, something went wrong: %@", [error localizedDescription]);
                if (weakSelf.recomCallback) {
                    weakSelf.recomCallback(nil, error);
                } if (weakSelf.qstsCategCallback) {
                    weakSelf.qstsCategCallback(nil, nil, error);
                }
            }];
            [weakSelf.operation start];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error){
            NSLog(@"Oops, something went wrong: %@", [error localizedDescription]);
            if (weakSelf.recomCallback) {
                weakSelf.recomCallback(nil, error);
            } if (weakSelf.qstsCategCallback) {
                weakSelf.qstsCategCallback(nil, nil, error);
            }
        }];
        [self.operation start];
    }
    
}

- (NSArray *)getQuestsForSearchString:(NSString *)string {
    NSArray *filteredArray = [self.questsArray mutableCopy];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains[c] %@ OR description contains[c] %@ OR company contains[c] %@ OR metro contains[c] %@", string, string, string, string];
    filteredArray = [NSMutableArray arrayWithArray:[self.questsArray filteredArrayUsingPredicate:predicate]];
    NSMutableArray *finalArray = [NSMutableArray new];
    for (NSDictionary *dic in filteredArray) {
        [finalArray addObject:self.questsDic[dic[@"id"]]];
    }
    return [NSArray arrayWithArray:finalArray];
}

- (UIImage *)getImageForUrl:(NSString *)url {
    if (self.imagesDictionary[url]) {
        return [[UIImage alloc] initWithData:self.imagesDictionary[url]];
    }
    return nil;
}

- (void)setImage:(UIImage *)image forUrl:(NSString *)url {
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSData *data = UIImageJPEGRepresentation(image, 1.0f);
        [self.imagesDictionary setObject:data forKey:url];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
        });
    });
}

- (void)saveImages {
    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:[NSDictionary dictionaryWithDictionary:self.imagesDictionary]];
    [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:[NSString stringWithFormat:@"imagesForCity%@", [[CitiesModel sharedInstance] selectedCity]]];
}

@end

