//
//  FeedTableViewController.m
//  World Of Quests
//
//  Created by Alexander on 01.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "FeedTableViewController.h"
#import "CityTableViewController.h"
#import "QuestTableViewController.h"
#import "RecomendedTableViewController.h"
#import "QuestTableViewCell.h"
#import "FilterView.h"
#import "QuestModel.h"
#import "FilterTableView.h"
#import "GlobalActivity.h"
#import "GlobalAlert.h"
#import "Reachability.h"

@interface FeedTableViewController () <FilterViewDelegate>

@property (strong, nonatomic) NSArray *categoriesArray;
@property (strong, nonatomic) NSDictionary *questsDic;
@property (strong, nonatomic) NSArray *filteredArray;
@property (strong, nonatomic) FilterTableView *ftv;
@property (strong, nonatomic) FilterView *fv;
@property (strong, nonatomic) UIView *alphaView;
@property (strong, nonatomic) NSNumber *curCategory;
@property (strong, nonatomic) UISearchBar *questSearchBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *searchItem;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelItem;
@property (strong, nonatomic) UILabel *pleaseWaitLabel;
@property (nonatomic) BOOL refreshing;
@property (strong, nonatomic) UILabel *noQuestsLabel;

@end

@implementation FeedTableViewController {
    UIView *tapView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initGui];
    [self loadQuests];
}

- (void)initGui {
    [self initAlphaView];
    [self initFilterTableView];
    [self initNavBar];
    [self initAlphaTap];
    [self initRefreshControl];
    [self initSearchController];
    [self initWaitLabel];
    [self initNoQuestsLabel];
}

- (void)initAlphaView {
    self.alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 64.0f, self.view.frame.size.width, self.view.frame.size.height)];
    self.alphaView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    self.alphaView.hidden = YES;
    self.alphaView.layer.masksToBounds = YES;
    [self.navigationController.view addSubview:self.alphaView];
}

- (void)initAlphaTap {
    tapView = [[UIView alloc] initWithFrame:CGRectMake(0, 44.0f * self.categoriesArray.count - 1, self.alphaView.frame.size.width, self.alphaView.frame.size.height)];
    tapView.backgroundColor = [UIColor clearColor];
    [self.alphaView addSubview:tapView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self.fv action:@selector(hideFilter)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [tapView addGestureRecognizer:tap];
}

- (void)initNavBar {
    self.fv = [[NSBundle mainBundle] loadNibNamed:@"FilterView" owner:self options:nil].lastObject;
    [self.fv setFrame:CGRectMake(0, 0, 220.0f, 44.0f)];
    self.fv.delegate = self;
    self.navigationItem.titleView = self.fv;
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.rightBarButtonItem = self.searchItem;
}

- (void)initFilterTableView {
    self.ftv = [[NSBundle mainBundle] loadNibNamed:@"FilterTableView" owner:self options:nil].lastObject;
    self.ftv.delegate = self;
    self.ftv.dataSource = self;
    self.ftv.frame = CGRectMake(0, -44.0f * self.categoriesArray.count, self.view.frame.size.width, 44.0f * self.categoriesArray.count - 1);
    [self.alphaView addSubview:self.ftv];
    self.ftv.hidden = NO;
    self.ftv.alpha = 1.0f;
}

- (void)initRefreshControl {
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl setTintColor:[UIColor whiteColor]];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
}

- (void)initSearchController {
    self.questSearchBar = [[UISearchBar alloc] init];
    [self.questSearchBar sizeToFit];
    self.questSearchBar.delegate = self;
    self.questSearchBar.translucent = NO;
    self.questSearchBar.barTintColor = [UIColor colorWithRed:71.0f/255.0f green:71.0f/255.0f blue:71.0f/255.0f alpha:1];
    [self.tableView reloadData];
}

- (void)initWaitLabel {
    self.pleaseWaitLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 45.0f)];
    self.pleaseWaitLabel.alpha = 0;
    self.pleaseWaitLabel.backgroundColor = [UIColor clearColor];
    self.pleaseWaitLabel.textColor = [UIColor whiteColor];
    self.pleaseWaitLabel.font = [UIFont systemFontOfSize:15.0f];
    self.pleaseWaitLabel.textAlignment = NSTextAlignmentCenter;
    self.pleaseWaitLabel.numberOfLines = 0;
    self.pleaseWaitLabel.text = @"Идет обновление данных. Это займет не больше минуты!";
    if (self.refreshing) {
        self.tableView.tableHeaderView = self.pleaseWaitLabel;
    }
}

- (void)initNoQuestsLabel {
    self.noQuestsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50.0f)];
    self.noQuestsLabel.textColor = [UIColor whiteColor];
    self.noQuestsLabel.font = [UIFont systemFontOfSize:15.0f];
    self.noQuestsLabel.textAlignment = NSTextAlignmentCenter;
    self.noQuestsLabel.text = @"Квесты в данном городе не найдены.";
}

- (void)loadQuests {
    [[GlobalActivity sharedInstance] showActivity];
    [[QuestModel sharedInstance] setQuestsWithCategoriesCallback:^(NSDictionary *questsDic, NSArray *categoriesArray, NSError *error) {
        if (!error) {
            [[GlobalActivity sharedInstance] hideActivity];
            if ([self.refreshControl isRefreshing]) {
                [self.refreshControl endRefreshing];
            }
            self.questsDic = questsDic;
            self.categoriesArray = categoriesArray;
            self.ftv.frame = CGRectMake(0, -44.0f * self.categoriesArray.count + 64.0f, self.view.frame.size.width, 44.0f * self.categoriesArray.count - 1);
            [self.ftv reloadData];
            [self.fv showFilter];
            [self.tableView addSubview:self.refreshControl];
            self.searchItem.enabled = YES;
            self.tableView.tableHeaderView = nil;
            tapView.frame = CGRectMake(0, 44.0f * self.categoriesArray.count - 1, self.alphaView.frame.size.width, self.alphaView.frame.size.height);
            if (!self.questsDic.count > 0) {
                self.tableView.tableHeaderView = self.noQuestsLabel;
            }
        } else {
            [[GlobalAlert sharedInstance] showNoDataAlert];
        }
    }];
}

- (void)refreshTable {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        [[GlobalAlert sharedInstance] showNoInternetAlert];
        [self.refreshControl endRefreshing];
    } else {
        self.refreshing = YES;
        self.searchItem.enabled = NO;
        self.questsDic = nil;
        self.categoriesArray = nil;
        [self.tableView reloadData];
        self.tableView.tableHeaderView = self.pleaseWaitLabel;
        [UIView animateWithDuration:0.3 animations:^{
            self.pleaseWaitLabel.alpha = 1.0f;
        }];
        [[QuestModel sharedInstance] clearAndReloadData];
        UINavigationController *nc = self.navigationController.tabBarController.viewControllers[0];
        FeedTableViewController *vc = nc.viewControllers[0];
        [vc reloadingData];
    }
}

- (void)reloadingData {
    self.questsDic = nil;
    self.categoriesArray = nil;
    self.searchItem.enabled = NO;
    self.tableView.tableHeaderView = self.pleaseWaitLabel;
    [self.tableView reloadData];
    [self.refreshControl removeFromSuperview];
    self.questSearchBar.text = @"";
    [self.questSearchBar resignFirstResponder];
    self.navigationItem.rightBarButtonItem = self.searchItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (Quest *)questForIndex:(NSInteger)index {
    Quest *quest = (Quest *)self.questsDic[self.categoriesArray[[self.curCategory integerValue]][@"quests"][index][@"id"]];
    return quest;
}

#pragma mark - Search Methods

- (IBAction)searchAction:(id)sender {
    [self.refreshControl removeFromSuperview];
    self.tableView.contentOffset = CGPointMake(0, 0);
    self.fv.hidden = YES;
    self.tableView.tableHeaderView = self.questSearchBar;
    self.navigationItem.rightBarButtonItem = self.cancelItem;
    [self.questSearchBar becomeFirstResponder];
}

- (IBAction)cancelAction:(id)sender {
    [self.tableView addSubview:self.refreshControl];
    self.fv.hidden = NO;
    self.filteredArray = nil;
    self.navigationItem.rightBarButtonItem = self.searchItem;
    self.questSearchBar.text = @"";
    [self.questSearchBar resignFirstResponder];
    self.tableView.tableHeaderView = nil;
    [self.tableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.filteredArray = [[QuestModel sharedInstance] getQuestsForSearchString:searchText];
    [self.tableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - TableView DataSource & Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.ftv && self.categoriesArray.count > 0) {
        return 1;
    } else if (self.questsDic.allKeys.count > 0 && self.curCategory) {
        return 1;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.ftv) {
        return self.categoriesArray.count;
    }
    if (!self.filteredArray) {
        return [(NSArray *)self.categoriesArray[[self.curCategory integerValue]][@"quests"] count];
    }
    return self.filteredArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.ftv) {
        return 44.0f;
    }
    Quest *curQuest;
    if (!self.filteredArray) {
        curQuest = [self questForIndex:indexPath.row];
    } else {
        curQuest = self.filteredArray[indexPath.row];
    }
    NSString *adressText;
    if (curQuest.metro) {
        adressText = [NSString stringWithFormat:@"%@\n%@", curQuest.adress, curQuest.metro];
    } else adressText = curQuest.adress;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    UIFont *fontSubject = [UIFont systemFontOfSize:21.0f];
    NSDictionary *attributesDictionarySubject = [NSDictionary dictionaryWithObjectsAndKeys:
                                                 fontSubject, NSFontAttributeName, nil];
    CGRect frameSubject = [curQuest.name boundingRectWithSize:CGSizeMake(screenWidth - 32.0f, 160.0f)
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                   attributes:attributesDictionarySubject
                                                      context:nil];
    fontSubject = [UIFont systemFontOfSize:16.0f];
    attributesDictionarySubject = [NSDictionary dictionaryWithObjectsAndKeys:
                                   fontSubject, NSFontAttributeName, nil];
    CGRect adresFrame = [adressText  boundingRectWithSize:CGSizeMake(screenWidth - 32.0f, 160.0f)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:attributesDictionarySubject
                                                  context:nil];
    NSInteger height = (screenWidth - 16) * 2 / 3 + frameSubject.size.height + adresFrame.size.height + 65.0f;
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.ftv) {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mainCell"];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.text = self.categoriesArray[indexPath.row][@"button_code"];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor whiteColor];
        if (self.ftv.selectedCategory && [self.ftv.selectedCategory integerValue] == indexPath.row) {
            [cell addSubview:self.ftv.checkImageView];
        }
        return cell;
    }
    Quest *curQuest;
    if (!self.filteredArray) {
        curQuest = [self questForIndex:indexPath.row];
    } else {
        curQuest = self.filteredArray[indexPath.row];
    }
    QuestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell" forIndexPath:indexPath];
    cell.curQuest = curQuest;
    Quest *cellQuest = (Quest *)cell.curQuest;
    cellQuest.curCell = cell;
    [cellQuest setupCell];
    return cell;
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(QuestTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView != self.ftv) {
        cell.questImageView.image = nil;
        Quest *cellQuest = (Quest *)cell.curQuest;
        [cellQuest pauseImageDownload];
        if (cellQuest) {
            cellQuest.curCell = nil;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.ftv) {
        [self.ftv deselectRowAtIndexPath:indexPath animated:YES];
        self.ftv.selectedCategory = [NSNumber numberWithInteger:indexPath.row];
        [self.ftv reloadData];
        self.fv.titleLabel.text = self.categoriesArray[indexPath.row][@"name"];
        [self.fv performSelector:@selector(hideFilter) withObject:nil afterDelay:0.8f];
        self.curCategory = [NSNumber numberWithInteger:indexPath.row];
        [self.tableView reloadData];
    } else {
        Quest *curQuest;
        if (!self.filteredArray) {
            curQuest = [self questForIndex:indexPath.row];
        } else {
            curQuest = self.filteredArray[indexPath.row];
        }
        QuestTableViewController *vc = (QuestTableViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"questController"];
        vc.curQuest = curQuest;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - FilterView Delegate Methods

- (void)filterWillShow {
    self.alphaView.hidden = NO;
    [UIView animateWithDuration:0.2f animations:^{
        self.alphaView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6f];
        self.ftv.center = CGPointMake(self.view.frame.size.width / 2, (44.0f * self.categoriesArray.count) / 2);
    }];
}

- (void)filterWillHide {
    [UIView animateWithDuration:0.2f animations:^{
        self.alphaView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
        self.ftv.center = CGPointMake(self.view.frame.size.width / 2, (-44.0f * self.categoriesArray.count) / 2);
    }completion:^(BOOL finished) {
        self.alphaView.hidden = YES;
    }];
}



@end
