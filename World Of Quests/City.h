//
//  City.h
//  World Of Quests
//
//  Created by Alexander on 13.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject

@property (strong, nonatomic) NSNumber *cityId;
@property (strong, nonatomic) NSNumber *sort;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *fullUrl;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *woqLink;
@property (nonatomic) BOOL isShowingInMenu;

+ (instancetype)cityFromDic:(NSDictionary *)dic;

@end
