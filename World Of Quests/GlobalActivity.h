//
//  GlobalActivity.h
//  World Of Quests
//
//  Created by Alexander on 06.01.16.
//  Copyright © 2016 Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GlobalActivity : NSObject

+ (instancetype)sharedInstance;
- (void)showActivity;
- (void)hideActivity;

@end
