//
//  QuestScreenTableViewCell.m
//  World Of Quests
//
//  Created by Alexander on 18.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "QuestScreenTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "URLManager.h"
#import "QuestModel.h"

@implementation QuestScreenTableViewCell

- (void)awakeFromNib {
    [self initGui];
}

- (void)initGui {
    self.prevImageButton.layer.cornerRadius = 16.0f;
    self.prevImageButton.layer.masksToBounds = YES;
    self.prevImageButton.layer.borderWidth = 1.0f;
    self.prevImageButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.nextImageButton.layer.cornerRadius = 16.0f;
    self.nextImageButton.layer.masksToBounds = YES;
    self.nextImageButton.layer.borderWidth = 1.0f;
    self.nextImageButton.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (void)setupCell {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    UIImageView *firstImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenWidth * (2.0f / 3.0f))];
    [self.questImageView addSubview:firstImgView];
    UIImage *camIcon = [UIImage imageNamed:@"camIcon"];
    if (self.curQuest.firstImage) {
        firstImgView.image = self.curQuest.firstImage;
    } else {
        firstImgView.image = camIcon;
    }
    
    UIImageView *secondImgView = [[UIImageView alloc] initWithFrame:CGRectMake(screenWidth, 0, screenWidth, screenWidth * (2.0f / 3.0f))];
    [self.questImageView addSubview:secondImgView];
    if (![[QuestModel sharedInstance] getImageForUrl:self.curQuest.photo2]) {
        [secondImgView sd_setImageWithURL:[[URLManager sharedInstance] urlWithPredicateFromString:self.curQuest.photo2] placeholderImage:camIcon completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [[QuestModel sharedInstance] setImage:image forUrl:self.curQuest.photo2];
        }];
    } else {
        secondImgView.image = [[QuestModel sharedInstance] getImageForUrl:self.curQuest.photo2];
    }
    
    UIImageView *thirdImgView = [[UIImageView alloc] initWithFrame:CGRectMake(2 * screenWidth, 0, screenWidth, screenWidth * (2.0f / 3.0f))];
    [self.questImageView addSubview:thirdImgView];
    if (![[QuestModel sharedInstance] getImageForUrl:self.curQuest.photo3]) {
        [thirdImgView sd_setImageWithURL:[[URLManager sharedInstance] urlWithPredicateFromString:self.curQuest.photo3] placeholderImage:camIcon completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [[QuestModel sharedInstance] setImage:image forUrl:self.curQuest.photo3];
    }];
    } else {
        thirdImgView.image = [[QuestModel sharedInstance] getImageForUrl:self.curQuest.photo3];
    }
    
    NSMutableString *difString = [NSMutableString new];
    for (NSInteger x = [self.curQuest.level integerValue]; x > 0; x--) {
        [difString appendString:@""];
    }
    self.difficultyLabel.text = [NSString stringWithString:difString];
    difString = [NSMutableString stringWithString:@""];
    for (NSInteger x = [self.curQuest.minPlayers integerValue]; x > 0; x--) {
        [difString appendString:@""];
    }
    [difString appendString:@" - "];
    for (NSInteger x = [self.curQuest.maxPlayers integerValue]; x > 0; x--) {
        [difString appendString:@""];
    }
    self.peopleLabel.text = [NSString stringWithString:difString];
    difString = [NSMutableString stringWithString:@""];
    self.durationLabel.text = [NSString stringWithFormat:@" %ld мин",(long)[self.curQuest.duration integerValue]];
    for (NSInteger x = [self.curQuest.rating integerValue]; x > 0; x--) {
        [difString appendString:@""];
    }
    if ((NSInteger)([self.curQuest.rating floatValue] * 10) % 10 > 0) {
        [difString appendString:@""];
    }
    NSInteger reviewsCount = self.curQuest.reviews.count;
    NSString *revString;
    if (reviewsCount % 100 == 1 || (reviewsCount % 100 > 20 && reviewsCount % 10 == 1)) {
        revString = @"отзыв";
    } else if ((reviewsCount % 100 < 5 || reviewsCount % 100 > 21) && reviewsCount % 10 > 1 && reviewsCount % 10 < 5) {
        revString = @"отзыва";
    } else {
        revString = @"отзывов";
    }
    self.ratingLabel.text = [NSString stringWithFormat:@"%@ (%lu %@)", difString, (long)reviewsCount, revString];
    self.nameLabel.text = self.curQuest.name;
    self.DescriptionLabel.text = self.curQuest.descriptionFull;
    self.phoneLabel.text = self.curQuest.phone;
    NSString *adressText;
    if (self.curQuest.metro) {
        adressText = [NSString stringWithFormat:@"%@\n%@", self.curQuest.adress, self.curQuest.metro];
    } else adressText = self.curQuest.adress;
    self.adressLabel.text = adressText;
    self.routeLabel.text = self.curQuest.route;
}

- (IBAction)prevAction:(id)sender {
    if (self.questImageView.bounds.origin.x > 0) {
        self.pageController.currentPage -= 1;
        CGRect bounds = self.questImageView.bounds;
        bounds.origin.x -= bounds.size.width;
        [UIView animateWithDuration:0.3 animations:^{
            self.questImageView.bounds = bounds;
        }];
    }
}

- (IBAction)nextAction:(id)sender {
    if (self.questImageView.bounds.origin.x < 2 * self.questImageView.bounds.size.width) {
        self.pageController.currentPage += 1;
        CGRect bounds = self.questImageView.bounds;
        bounds.origin.x += bounds.size.width;
        [UIView animateWithDuration:0.3 animations:^{
            self.questImageView.bounds = bounds;
        }];
    }
}

+ (CGFloat)getRowHeightForQuest:(Quest *)quest {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    NSString *adressText;
    if (quest.metro) {
        adressText = [NSString stringWithFormat:@"%@\n%@", quest.adress, quest.metro];
    } else adressText = quest.adress;
    
    
    UIFont *fontSubject = [UIFont fontWithName:@"OpenSans" size:21.0f];
    NSDictionary *attributesDictionarySubject = [NSDictionary dictionaryWithObjectsAndKeys:
                                                 fontSubject, NSFontAttributeName, nil];
    CGRect nameFrame = [quest.name  boundingRectWithSize:CGSizeMake(screenWidth - 16.0f, 128.0f)
                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                attributes:attributesDictionarySubject
                                                                   context:nil];
    fontSubject = [UIFont  fontWithName:@"OpenSans" size:15.0f];
    attributesDictionarySubject = [NSDictionary dictionaryWithObjectsAndKeys:
                                                 fontSubject, NSFontAttributeName, nil];
    CGRect descriptionFrame = [quest.descriptionFull  boundingRectWithSize:CGSizeMake(screenWidth - 16.0f, 1024.0f)
                                                            options:NSStringDrawingUsesLineFragmentOrigin
                                                         attributes:attributesDictionarySubject
                                                            context:nil];
    CGRect phoneFrame = [quest.phone  boundingRectWithSize:CGSizeMake(screenWidth - 16.0f, 128.0f)
                                                               options:NSStringDrawingUsesLineFragmentOrigin
                                                            attributes:attributesDictionarySubject
                                                               context:nil];
    CGRect adressFrame = [adressText  boundingRectWithSize:CGSizeMake(screenWidth - 16.0f, 128.0f)
                                                               options:NSStringDrawingUsesLineFragmentOrigin
                                                            attributes:attributesDictionarySubject
                                                               context:nil];
    CGRect routeFrame = [quest.route  boundingRectWithSize:CGSizeMake(screenWidth - 16.0f, 128.0f)
                                                               options:NSStringDrawingUsesLineFragmentOrigin
                                                            attributes:attributesDictionarySubject
                                                               context:nil];
    if ([quest.route isEqualToString:@""]) {
        routeFrame.size.height = -14.0f;
    }
    return 267.0f + screenWidth * (2.0f / 3.0f) + nameFrame.size.height + descriptionFrame.size.height + phoneFrame.size.height + adressFrame.size.height + routeFrame.size.height;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
