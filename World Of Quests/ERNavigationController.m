//
//  ERViewController.m
//  Eristica
//
//  Created by Алексей Левинтов on 06.03.15.
//  Copyright (c) 2015 startIndustries. All rights reserved.
//

#import "ERNavigationController.h"

@interface ERNavigationController () <UINavigationControllerDelegate>

@end

@implementation ERNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    viewController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    if (!viewController.navigationItem.titleView) {
        if (viewController.navigationItem.title) {
            CGRect rect;
            UIFont *font = [UIFont fontWithName:@"P22UndergroundCYPro-Book" size:20.0];
            NSDictionary *attributes = @{NSFontAttributeName:font};
            rect.origin = CGPointZero;
            rect.size = [viewController.navigationItem.title sizeWithAttributes:attributes];
            UILabel *label = [[UILabel alloc] initWithFrame:rect];
            label.text = viewController.navigationItem.title;
            label.font = font;
            label.textColor = [UIColor whiteColor];
            viewController.navigationItem.titleView = label;
        }
    }
}

-(BOOL)shouldAutorotate {
    return NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
