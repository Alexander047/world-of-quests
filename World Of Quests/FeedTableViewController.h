//
//  FeedTableViewController.h
//  World Of Quests
//
//  Created by Alexander on 01.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedTableViewController : UITableViewController <UISearchBarDelegate>

- (void)reloadingData;

@end
