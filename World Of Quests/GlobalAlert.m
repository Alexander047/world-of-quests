//
//  GlobalAlert.m
//  World Of Quests
//
//  Created by Alexander on 06.01.16.
//  Copyright © 2016 Alexander. All rights reserved.
//

#import "GlobalAlert.h"

@implementation GlobalAlert {
    UIAlertView *alert;
}

+ (instancetype)sharedInstance {
    static GlobalAlert *model = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        model = [GlobalAlert new];
        [model initGui];
    });
    return model;
}

- (void)initGui {
    alert = [[UIAlertView alloc] initWithTitle:@"Ошибка!" message:@"Не удалось получить данные. Проверьте подключение к интернету или попробуйте позже." delegate:self cancelButtonTitle:@"Хорошо" otherButtonTitles:nil];
}

- (void)showNoDataAlert {
    alert.title = @"Ошибка!";
    alert.message = @"Не удалось получить данные. Проверьте подключение к интернету или попробуйте позже.";
    [alert show];
}

- (void)showNoInternetAlert {
    alert.title = @"";
    alert.message = @"Нет подключения к интернету.";
    [alert show];
}

@end
