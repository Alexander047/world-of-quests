//
//  UIFont+SystemFontOverride.h
//  MAI Mobile
//
//  Created by Alexander on 29.05.15.
//  Copyright (c) 2015 Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (SytemFontOverride)

+ (UIFont *)boldSystemFontOfSize:(CGFloat)fontSize;
+ (UIFont *)systemFontOfSize:(CGFloat)fontSize;

@end
