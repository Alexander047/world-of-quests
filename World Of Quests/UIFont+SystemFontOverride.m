//
//  UIFont+SystemFontOverride.m
//  MAI Mobile
//
//  Created by Alexander on 29.05.15.
//  Copyright (c) 2015 Alexander. All rights reserved.
//

#import "UIFont+SystemFontOverride.h"

@implementation UIFont (SytemFontOverride)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

+ (UIFont *)boldSystemFontOfSize:(CGFloat)fontSize {
    UIFont *font = [UIFont fontWithName:@"OpenSans-Bold" size:fontSize];
    return font;
}

+ (UIFont *)systemFontOfSize:(CGFloat)fontSize {
    UIFont *font = [UIFont fontWithName:@"OpenSans" size:fontSize];
    return font;
}

#pragma clang diagnostic pop

@end
