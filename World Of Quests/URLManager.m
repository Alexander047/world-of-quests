//
//  URLManager.m
//  World Of Quests
//
//  Created by Alexander on 14.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "URLManager.h"

@interface URLManager ()

@property (strong, nonatomic) NSString *predicate;

@end

@implementation URLManager

+ (instancetype)sharedInstance {
    static URLManager *manager = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        manager = [URLManager new];
        if ([[NSUserDefaults standardUserDefaults] valueForKey:@"urlPredicate"]) {
            manager.predicate = [[NSUserDefaults standardUserDefaults] valueForKey:@"urlPredicate"];
        } else {
            manager.predicate = @"http://";
        }
    });
    return manager;
}

- (NSURL *)urlWithPredicateFromString:(NSString *)string {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", self.predicate, string]];
    return url;
}

- (void)updatePredicate:(NSString *)predicate {
    self.predicate = predicate;
    [[NSUserDefaults standardUserDefaults] setValue:predicate forKey:@"urlPredicate"];
}

@end
