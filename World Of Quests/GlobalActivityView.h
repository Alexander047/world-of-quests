//
//  GlobalActivityView.h
//  World Of Quests
//
//  Created by Alexander on 16.01.16.
//  Copyright © 2016 Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlobalActivityView : UIView

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
