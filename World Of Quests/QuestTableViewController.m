//
//  QuestTableViewController.m
//  World Of Quests
//
//  Created by Alexander on 19.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "QuestTableViewController.h"
#import "QuestScreenTableViewCell.h"
#import "ReviewTableViewCell.h"
#import "GlobalActivity.h"
#import "GlobalAlert.h"

@interface QuestTableViewController ()

@end

@implementation QuestTableViewController {
    UIImageView *reviewsArrowImageView;
    BOOL reviewsOpen;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ReviewTableViewCell" bundle:nil] forCellReuseIdentifier:@"reviewCell"];
    [self initGui];
}

- (void)initGui {
    [self initNavBar];
    [self initArraws];
}

- (void)initNavBar {
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160.0f, 30.0f)];
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor whiteColor];
    navLabel.font = [UIFont systemFontOfSize:16.0f];
    navLabel.text = self.curQuest.name;
    navLabel.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView = navLabel;
}

- (void)initArraws {
    reviewsArrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"downArrowIcon"]];
    reviewsArrowImageView.center = CGPointMake([[UIScreen mainScreen] bounds].size.width / 2 + 48.0f, 25.0f);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.curQuest.bookingLink) {
        return 3;
    } return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 1 && reviewsOpen) {
        return self.curQuest.reviews.count + 1;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return [QuestScreenTableViewCell getRowHeightForQuest:self.curQuest];
            break;
        case 1:
            if (indexPath.row == 0) {
                return 50.0f;
            }
            return [ReviewTableViewCell getRowHeightForReview:self.curQuest.reviews[indexPath.row - 1]];
            break;
        case 2:
            return 72.0f;
            break;
        case 3:
            return 72.0f;
            break;
        default:
            return 50;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        QuestScreenTableViewCell *questCell = (QuestScreenTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:@"questCell"];
        questCell.curQuest = self.curQuest;
        [questCell setupCell];
        cell = questCell;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"menuCell"];
            cell.textLabel.text = @"Отзывы";
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.textLabel.font = [UIFont systemFontOfSize:19.0f];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            [cell addSubview:reviewsArrowImageView];
        } else {
            ReviewTableViewCell *reviewCell = [self.tableView dequeueReusableCellWithIdentifier:@"reviewCell"];
            [reviewCell setupCellWithReview:self.curQuest.reviews[indexPath.row - 1]];
            cell = reviewCell;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    } else {
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"buyCell"];
        for (UIView *view in cell.contentView.subviews) {
            if ([view class] == [UIButton class]) {
                [view.layer setCornerRadius:4.0f];
                [view.layer setMasksToBounds:YES];
                UIButton *btn = (UIButton *)view;
                [btn addTarget:self action:@selector(openSafari) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (void)openSafari {
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:self.curQuest.bookingLink]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.curQuest.bookingLink]];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1 && indexPath.row == 0) {
        if (!reviewsOpen) {
            reviewsOpen = YES;
            [reviewsArrowImageView setTransform:CGAffineTransformMakeRotation(M_PI)];
            [self.tableView reloadData];
        } else {
            reviewsOpen = NO;
            [reviewsArrowImageView setTransform:CGAffineTransformMakeRotation(0)];
            [self.tableView reloadData];
        }
    }
}

@end
