//
//  CitiesModel.m
//  World Of Quests
//
//  Created by Alexander on 13.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "CitiesModel.h"
#import <AFNetworking/AFNetworking.h>
#import "City.h"

@implementation CitiesModel

+ (instancetype)sharedInstance {
    static CitiesModel *model = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        model = [CitiesModel new];
    });
    return model;
}

- (void)getCountriesAndCitiesWithCallback:(void (^)(NSArray *, NSError *))callback {
    NSData *arrayData = [[NSUserDefaults standardUserDefaults] objectForKey:@"citiesData"];
    NSArray *dataArray = [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];
    
    if (!dataArray) {
        [self updateCountriesAndCitiesWithCallback:^(NSArray *array, NSError *error) {
            callback(array, error);
        }];
    } else {
        NSMutableArray *finalArray = [NSMutableArray new];
        for (NSDictionary *citiesDic in dataArray) {
            NSMutableArray *curCities = [NSMutableArray new];
            for (NSDictionary *dic in citiesDic[@"cities"]) {
                City *city = [City cityFromDic:dic];
                [curCities addObject:city];
            }
            [finalArray addObject:@{@"country" : citiesDic[@"name"], @"cities" : curCities}];
        }
        callback(finalArray, nil);
    }
}

- (void)updateCountriesAndCitiesWithCallback:(void (^)(NSArray *, NSError *))callback {
    NSURL *url = [NSURL URLWithString:@"http://mir-kvestov.ru/app_api/countries.json"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *countriesArray = responseObject;
        
        NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:countriesArray];
        [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"citiesData"];
        
        NSMutableArray *finalArray = [NSMutableArray new];
        for (NSDictionary *citiesDic in countriesArray) {
            NSMutableArray *curCities = [NSMutableArray new];
            for (NSDictionary *dic in citiesDic[@"cities"]) {
                City *city = [City cityFromDic:dic];
                [curCities addObject:city];
            }
            [finalArray addObject:@{@"country" : citiesDic[@"name"], @"cities" : curCities}];
        }
        callback(finalArray, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        NSLog(@"Oops, something went wrong: %@", [error localizedDescription]);
        callback(nil, error);
    }];
    [operation start];
}

- (void)selectCity:(NSNumber *)cityId {
    [[NSUserDefaults standardUserDefaults] setValue:cityId forKey:@"selectedCityId"];
}

- (NSNumber *)selectedCity {
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"selectedCityId"]) {
        return (NSNumber *)[[NSUserDefaults standardUserDefaults] valueForKey:@"selectedCityId"];
    }
    return nil;
}

@end
