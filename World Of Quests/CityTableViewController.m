//
//  ViewController.m
//  World Of Quests
//
//  Created by Alexander on 23.11.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "CityTableViewController.h"
#import "CitiesModel.h"
#import "City.h"
#import "TabBarViewController.h"
#import "URLManager.h"
#import "QuestModel.h"
#import "GlobalActivity.h"
#import "GlobalAlert.h"

@interface CityTableViewController ()

@property (strong, nonatomic) NSArray <NSDictionary *> *citiesArray;

@end

@implementation CityTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self checkSelectedCity];
    [self initNavBar];
    [self initRefreshControl];
    [self loadData];
}

- (void)checkSelectedCity {
    if (!self.tabBarController && [[CitiesModel sharedInstance] selectedCity]) {
        TabBarViewController *tc = [self.storyboard instantiateViewControllerWithIdentifier:@"tabBar"];
        [self.navigationController pushViewController:tc animated:NO];
        self.navigationController.navigationBarHidden = YES;
    }
}

- (void)initRefreshControl {
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl setTintColor:[UIColor whiteColor]];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
}

- (void)initNavBar {
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160.0f, 30.0f)];
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor whiteColor];
    navLabel.font = [UIFont systemFontOfSize:16.0f];
    navLabel.text = @"Ваш город";
    navLabel.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView = navLabel;
}

- (void)refreshTable {
    [[CitiesModel sharedInstance] updateCountriesAndCitiesWithCallback:^(NSArray *array, NSError *error) {
        if ([self.refreshControl isRefreshing]) {
            [self.refreshControl endRefreshing];
        }
        if (!error) {
            self.citiesArray = array;
            [self.tableView reloadData];
        } else {
            [[GlobalAlert sharedInstance] showNoDataAlert];
        }
    }];
}

- (void)loadData {
    [[GlobalActivity sharedInstance] showActivity];
    [[CitiesModel sharedInstance] getCountriesAndCitiesWithCallback:^(NSArray *array, NSError *error) {
        if (!error) {
            [[GlobalActivity sharedInstance] hideActivity];
            self.citiesArray = array;
            [self.tableView reloadData];
        } else {
            [[GlobalAlert sharedInstance] showNoDataAlert];
        }
    }];
    
}

#pragma mark - TableView Data Source & Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.citiesArray.count > 0) {
        return  self.citiesArray.count;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([(NSArray*)self.citiesArray[section][@"cities"] count] > 0) {
        return [(NSArray*)self.citiesArray[section][@"cities"] count];
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 28.0f)];
    view.backgroundColor = [UIColor colorWithRed:107.0f/255.0f green:107.0f/255.0f blue:107.0f/255.0f alpha:1.0f];
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(8.0f, 0, 320.0f, 28.0f)];
    label.textColor = [UIColor whiteColor];
    label.text = (NSString*)self.citiesArray[section][@"country"];
    [view addSubview:label];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell"];
    cell.textLabel.text = [(City*)self.citiesArray[indexPath.section][@"cities"][indexPath.row] name];
    if ([[[CitiesModel sharedInstance] selectedCity] isEqualToNumber:[(City*)self.citiesArray[indexPath.section][@"cities"][indexPath.row] cityId]]) {
            UIImageView *checkView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkIcon"]];
            cell.accessoryView = checkView;
    } else {
        cell.accessoryView = nil;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    City *curCity = self.citiesArray[indexPath.section][@"cities"][indexPath.row];
    [[URLManager sharedInstance] updatePredicate:curCity.fullUrl];
    [[CitiesModel sharedInstance] selectCity:curCity.cityId];
    [self.tableView reloadData];
    [self performSelector:@selector(citySelected) withObject:nil afterDelay:0.8f];
}

- (void)citySelected {
    [[QuestModel sharedInstance] clearAndReloadData];
    if (!self.tabBarController) {
        TabBarViewController *tc = [self.storyboard instantiateViewControllerWithIdentifier:@"tabBar"];
        [self.navigationController pushViewController:tc animated:YES];
        self.navigationController.navigationBarHidden = YES;
    } else {
        [[GlobalActivity sharedInstance] showActivity];
        [self.tabBarController setSelectedIndex:0];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
