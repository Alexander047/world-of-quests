//
//  CitiesModel.h
//  World Of Quests
//
//  Created by Alexander on 13.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CitiesModel : NSObject

+ (instancetype)sharedInstance;
- (void)getCountriesAndCitiesWithCallback:(void(^)(NSArray *array, NSError *error))callback;
- (void)updateCountriesAndCitiesWithCallback:(void(^)(NSArray *array, NSError *error))callback;
- (void)selectCity:(NSNumber *)cityId;
- (NSNumber *)selectedCity;

@end
