//
//  ReviewTableViewCell.m
//  World Of Quests
//
//  Created by Alexander on 09.01.16.
//  Copyright © 2016 Alexander. All rights reserved.
//

#import "ReviewTableViewCell.h"

@implementation ReviewTableViewCell

- (void)awakeFromNib {
    self.whiteView.layer.cornerRadius = 8.0f;
    self.whiteView.layer.masksToBounds = YES;
}

- (void)setupCellWithReview:(NSDictionary *)review {
    NSMutableString *difString = [NSMutableString new];
    for (NSInteger x = [review[@"mark"] integerValue]; x > 0; x--) {
        [difString appendString:@""];
    }
    self.ratingLabel.text = difString;
    self.reviewTextLabel.text = review[@"comment"];
    self.userNameLabel.text = [NSString stringWithFormat:@"— %@ —", review[@"name"]];
}

+ (CGFloat)getRowHeightForReview:(NSDictionary *)review {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    UIFont *fontSubject = [UIFont fontWithName:@"OpenSans" size:16.0f];
    NSDictionary *attributesDictionarySubject = [NSDictionary dictionaryWithObjectsAndKeys:
                                                 fontSubject, NSFontAttributeName, nil];
    CGRect frameSubject = [review[@"comment"]  boundingRectWithSize:CGSizeMake(screenWidth - 48.0f, 1024.0f)
                                                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                                                              attributes:attributesDictionarySubject
                                                                                 context:nil];
    return 120.0f + frameSubject.size.height;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
