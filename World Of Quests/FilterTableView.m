//
//  FilterTableView.m
//  World Of Quests
//
//  Created by Alexander on 21.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "FilterTableView.h"

@implementation FilterTableView

- (void)awakeFromNib {
    self.scrollEnabled = NO;
    if (!self.checkImageView) {
        self.checkImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkIcon"]];
        self.checkImageView.center = CGPointMake([[UIScreen mainScreen] bounds].size.width - 20.0f, 22.0f);
    }
}

@end
