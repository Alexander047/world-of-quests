//
//  QuestModel.h
//  World Of Quests
//
//  Created by Alexander on 17.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface QuestModel : NSObject

+ (instancetype)sharedInstance;
- (void)clearAndReloadData;
- (void)setRecommendedCallback:(void(^)(NSArray *recommendedArray, NSError *error))callback;
- (void)setQuestsWithCategoriesCallback:(void(^)(NSDictionary *questsDic, NSArray *categArray, NSError *error))callback;
- (NSArray *)getQuestsForSearchString:(NSString *)string;
- (void)setImage:(UIImage *)image forUrl:(NSString *)url;
- (UIImage *)getImageForUrl:(NSString *)url;
- (void)saveImages;

@end
