//
//  RecomendedTableViewController.m
//  World Of Quests
//
//  Created by Alexander on 21.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "RecomendedTableViewController.h"
#import "CityTableViewController.h"
#import "QuestTableViewController.h"
#import "FeedTableViewController.h"
#import "QuestTableViewCell.h"
#import "QuestModel.h"
#import "Quest.h"
#import "GlobalActivity.h"
#import "GlobalAlert.h"
#import "Reachability.h"

@interface RecomendedTableViewController ()

@property (strong, nonatomic) NSArray <Quest *> *questsArray;
@property (strong, nonatomic) NSArray <Quest *> *filteredArray;
@property (strong, nonatomic) UISearchBar *questSearchBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *searchItem;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelItem;
@property (strong, nonatomic) UILabel *pleaseWaitLabel;
@property (strong, nonatomic) UILabel *noQuestsLabel;

@end

@implementation RecomendedTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initGui];
    [self loadData];
}

- (void)initGui {
    [self initNavBar];
    [self initRefreshControl];
    [self initSearchController];
    [self initWaitLabel];
    [self initNoQuestsLabel];
}

- (void)initRefreshControl {
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl setTintColor:[UIColor whiteColor]];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
}

- (void)initNavBar {
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160.0f, 30.0f)];
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor whiteColor];
    navLabel.font = [UIFont systemFontOfSize:16.0f];
    navLabel.text = @"Рекомендуемые";
    navLabel.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView = navLabel;
    
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.rightBarButtonItem = self.searchItem;
}

- (void)initWaitLabel {
    self.pleaseWaitLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 45.0f)];
    self.pleaseWaitLabel.alpha = 0;
    self.pleaseWaitLabel.backgroundColor = [UIColor clearColor];
    self.pleaseWaitLabel.textColor = [UIColor whiteColor];
    self.pleaseWaitLabel.font = [UIFont systemFontOfSize:15.0f];
    self.pleaseWaitLabel.textAlignment = NSTextAlignmentCenter;
    self.pleaseWaitLabel.numberOfLines = 0;
    self.pleaseWaitLabel.text = @"Идет обновление данных. Это займет не больше минуты!";
}

- (void)initNoQuestsLabel {
    self.noQuestsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50.0f)];
    self.noQuestsLabel.textColor = [UIColor whiteColor];
    self.noQuestsLabel.font = [UIFont systemFontOfSize:15.0f];
    self.noQuestsLabel.textAlignment = NSTextAlignmentCenter;
    self.noQuestsLabel.text = @"Квесты в данном городе не найдены.";
}

- (void)loadData {
    [[GlobalActivity sharedInstance] showActivity];
    [[QuestModel sharedInstance] setRecommendedCallback:^(NSArray *recommendedArray, NSError *error) {
        if (!error) {
            [[GlobalActivity sharedInstance] hideActivity];
            self.questsArray = recommendedArray;
            if ([self.refreshControl isRefreshing]) {
                [self.refreshControl endRefreshing];
            }
            [self.tableView addSubview:self.refreshControl];
            self.searchItem.enabled = YES;
            self.tableView.tableHeaderView = nil;
            [self.tableView reloadData];
            if (!self.questsArray.count > 0) {
                self.tableView.tableHeaderView = self.noQuestsLabel;
            }
        } else {
            [[GlobalAlert sharedInstance] showNoDataAlert];
        }
    }];
}

- (void)initSearchController {
    self.questSearchBar = [[UISearchBar alloc] init];
    [self.questSearchBar sizeToFit];
    self.questSearchBar.delegate = self;
    self.questSearchBar.translucent = NO;
    self.questSearchBar.barTintColor = [UIColor colorWithRed:71.0f/255.0f green:71.0f/255.0f blue:71.0f/255.0f alpha:1];
    [self.tableView reloadData];
}

- (void)refreshTable {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        [[GlobalAlert sharedInstance] showNoInternetAlert];
        [self.refreshControl endRefreshing];
    } else {
        self.searchItem.enabled = NO;
        self.questsArray = nil;
        [self.tableView reloadData];
        self.tableView.tableHeaderView = self.pleaseWaitLabel;
        [UIView animateWithDuration:0.3 animations:^{
            self.pleaseWaitLabel.alpha = 1.0f;
        }];
        [[QuestModel sharedInstance] clearAndReloadData];
        UINavigationController *nc = self.navigationController.tabBarController.viewControllers[1];
        FeedTableViewController *vc = nc.viewControllers[0];
        [vc reloadingData];
    }
}

- (void)reloadingData {
    self.questsArray = nil;
    self.searchItem.enabled = NO;
    self.tableView.tableHeaderView = self.pleaseWaitLabel;
    [self.tableView reloadData];
    [self.refreshControl removeFromSuperview];
    self.questSearchBar.text = @"";
    [self.questSearchBar resignFirstResponder];
    self.navigationItem.rightBarButtonItem = self.searchItem;
}

- (void)showNoQuestsView {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Search Controller Protocols

- (IBAction)searchAction:(id)sender {
    [self.refreshControl removeFromSuperview];
    self.tableView.contentOffset = CGPointMake(0, 0);
    self.tableView.tableHeaderView = self.questSearchBar;
    self.navigationItem.rightBarButtonItem = self.cancelItem;
    [self.questSearchBar becomeFirstResponder];
}

- (IBAction)cancelAction:(id)sender {
    [self.tableView addSubview:self.refreshControl];
    self.filteredArray = nil;
    self.navigationItem.rightBarButtonItem = self.searchItem;
    self.questSearchBar.text = @"";
    [self.questSearchBar resignFirstResponder];
    self.tableView.tableHeaderView = nil;
    [self.tableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.filteredArray = [[QuestModel sharedInstance] getQuestsForSearchString:searchText];
    [self.tableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.questsArray.count > 0) {
        return 1;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.filteredArray) {
        return self.filteredArray.count;
    }
    return self.questsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Quest *curQuest;
    if (self.filteredArray) {
        curQuest = self.filteredArray[indexPath.row];
    } else {
        curQuest = self.questsArray[indexPath.row];
    }
    NSString *adressText;
    if (curQuest.metro) {
        adressText = [NSString stringWithFormat:@"%@\n%@", curQuest.adress, curQuest.metro];
    } else adressText = curQuest.adress;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    UIFont *fontSubject = [UIFont systemFontOfSize:21.0f];
    NSDictionary *attributesDictionarySubject = [NSDictionary dictionaryWithObjectsAndKeys:
                                                 fontSubject, NSFontAttributeName, nil];
    CGRect frameSubject = [curQuest.name  boundingRectWithSize:CGSizeMake(screenWidth - 32.0f, 160.0f)
                                                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                                                              attributes:attributesDictionarySubject
                                                                                 context:nil];
    fontSubject = [UIFont systemFontOfSize:16.0f];
    attributesDictionarySubject = [NSDictionary dictionaryWithObjectsAndKeys:
                                                 fontSubject, NSFontAttributeName, nil];
    CGRect adresFrame = [adressText  boundingRectWithSize:CGSizeMake(screenWidth - 32.0f, 160.0f)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                    attributes:attributesDictionarySubject
                                                       context:nil];
    NSInteger height = (screenWidth - 16) * 2 / 3 + frameSubject.size.height + adresFrame.size.height + 65.0f;
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Quest *curQuest;
    if (self.filteredArray) {
        curQuest = self.filteredArray[indexPath.row];
    } else {
        curQuest = self.questsArray[indexPath.row];
    }
    QuestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell" forIndexPath:indexPath];
    cell.curQuest = curQuest;
    Quest *cellQuest = (Quest *)cell.curQuest;
    cellQuest.curCell = cell;
    [cellQuest setupCell];
    return cell;
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(QuestTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.questImageView.image = nil;
    Quest *cellQuest = (Quest *)cell.curQuest;
    [cellQuest pauseImageDownload];
    if (cellQuest) {
        cellQuest.curCell = nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Quest *curQuest;
    if (self.filteredArray) {
        curQuest = self.filteredArray[indexPath.row];
    } else {
        curQuest = self.questsArray[indexPath.row];
    }
    QuestTableViewController *vc = (QuestTableViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"questController"];
    vc.curQuest = curQuest;
    [self.navigationController pushViewController:vc animated:YES];
}



@end
