//
//  ReviewTableViewCell.h
//  World Of Quests
//
//  Created by Alexander on 09.01.16.
//  Copyright © 2016 Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIView *whiteView;

- (void)setupCellWithReview:(NSDictionary *)review;
+ (CGFloat)getRowHeightForReview:(NSDictionary *)review;

@end
