//
//  Quest.m
//  World Of Quests
//
//  Created by Alexander on 17.12.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "Quest.h"
#import <AFNetworking/AFNetworking.h>
#import "URLManager.h"
#import "QuestModel.h"

@implementation Quest {
    AFHTTPRequestOperation *operation;
}

+ (instancetype)questFromDic:(NSDictionary *)dic {
    Quest *curQuest = [Quest new];
    curQuest.name = dic[@"name"];
    curQuest.questId = dic[@"id"];
    curQuest.descriptionShort = dic[@"description_short"];
    curQuest.descriptionFull = dic[@"description"];
    curQuest.level = dic[@"level"];
    curQuest.minPlayers = dic[@"min_player"];
    curQuest.maxPlayers = dic[@"max_player"];
    curQuest.duration = dic[@"time"];
    curQuest.rating = dic[@"avg_mark"];
    curQuest.sortApp = dic[@"sort_app"];
    curQuest.photo1 = dic[@"photo1"];
    curQuest.photo2 = dic[@"photo2"];
    curQuest.photo3 = dic[@"photo3"];
    curQuest.url = dic[@"url"];
    curQuest.reviews = dic[@"reviews"];
    curQuest.phone = dic[@"phone"];
    curQuest.company = dic[@"company"];
    curQuest.adress = dic[@"address"];
    curQuest.route = dic[@"route"];
    curQuest.longitude = dic[@"longitude"];
    curQuest.latitude = dic[@"latitude"];
    curQuest.www = dic[@"www"];
    curQuest.categories = dic[@"categories"];
    curQuest.bookingLink = dic[@"booking_link"];
    curQuest.metro = dic[@"metro"];
    
    return curQuest;
}

- (void)setupCell {
    QuestTableViewCell *cell = (QuestTableViewCell *)self.curCell;
    cell.dificultyLabel.text = @"";
    cell.peopleLabel.text = @"";
    cell.timeLabel.text = @"";
    cell.titleLabel.text = @"";
    cell.adressLabel.text = @"";
    cell.ratingLabel.text = @"";
    NSMutableString *difString = [NSMutableString new];
    for (NSInteger x = [self.level integerValue]; x > 0; x--) {
        [difString appendString:@""];
    }
    cell.dificultyLabel.text = [NSString stringWithString:difString];
    difString = [NSMutableString stringWithString:@""];
    for (NSInteger x = [self.minPlayers integerValue]; x > 0; x--) {
        [difString appendString:@""];
    }
    [difString appendString:@" - "];
    for (NSInteger x = [self.maxPlayers integerValue]; x > 0; x--) {
        [difString appendString:@""];
    }
    cell.peopleLabel.text = [NSString stringWithString:difString];
    difString = [NSMutableString stringWithString:@""];
    cell.timeLabel.text = [NSString stringWithFormat:@" %ld мин",(long)[self.duration integerValue]];
    cell.titleLabel.text = self.name;
    NSString *adressText;
    if (self.metro) {
        adressText = [NSString stringWithFormat:@"%@\n%@", self.adress, self.metro];
    } else adressText = self.adress;
    cell.adressLabel.text = adressText;
    for (NSInteger x = [self.rating integerValue]; x > 0; x--) {
        [difString appendString:@""];
    }
    if ((NSInteger)([self.rating floatValue] * 10) % 10 > 0) {
        [difString appendString:@""];
    }
    NSInteger reviewsCount = self.reviews.count;
    NSString *revString;
    if (reviewsCount % 100 == 1 || (reviewsCount % 100 > 20 && reviewsCount % 10 == 1)) {
        revString = @"отзыв";
    } else if ((reviewsCount % 100 < 5 || reviewsCount % 100 > 21) && reviewsCount % 10 > 1 && reviewsCount % 10 < 5) {
        revString = @"отзыва";
    } else {
        revString = @"отзывов";
    }
    cell.ratingLabel.text = [NSString stringWithFormat:@"%@ (%lu %@)", difString, (long)reviewsCount, revString];
    if (!self.firstImage && !operation) {
        [self startImageDownload];
    } else if (!self.firstImage && operation) {
        [operation start];
    } else if (self.firstImage) {
        cell.questImageView.image = self.firstImage;
    }
    [self startImageDownload];
}

- (void)startImageDownload {
    __weak Quest *curQuest = self;
    if (![[QuestModel sharedInstance] getImageForUrl:self.photo1]) {
        NSURL *url = [[URLManager sharedInstance] urlWithPredicateFromString:self.photo1];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        AFImageResponseSerializer *serializer = [[AFImageResponseSerializer alloc] init];
        serializer.acceptableContentTypes = [serializer.acceptableContentTypes setByAddingObject:@"image/jpeg"];
        operation.responseSerializer = serializer;
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            curQuest.firstImage = responseObject;
            [[QuestModel sharedInstance] setImage:(UIImage *)responseObject forUrl:curQuest.photo1];
            QuestTableViewCell *cell = (QuestTableViewCell *)curQuest.curCell;
            if (cell) {
                cell.questImageView.image = (UIImage *)responseObject;
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error){
            QuestTableViewCell *cell = (QuestTableViewCell *)curQuest.curCell;
            cell.questImageView.image = [UIImage imageNamed:@"camIcon"];
        }];
        [operation start];
    } else {
        self.firstImage = [[QuestModel sharedInstance] getImageForUrl:self.photo1];
        QuestTableViewCell *cell = (QuestTableViewCell *)curQuest.curCell;
        if (cell) {
            cell.questImageView.image = self.firstImage;
        }
    }
}

- (void)pauseImageDownload {
    [operation pause];
}

@end
